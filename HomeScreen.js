import React from 'react';
import { StyleSheet, Text, View, FlatList, Dimensions, RefreshControl } from 'react-native';
let { width, height } = Dimensions.get('screen')
import { connect } from 'react-redux'

import { fetchProduct } from './API'
import { FetchProduct2 } from './HomeAction'

let addIndex = 0

class HomeScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      products: [],
      products2: [],
      refreshing: false
    }
  }



  componentDidMount() {
    const { onGetProduct, onFetchProduct } = this.props
    //onGetProduct()
    onFetchProduct()
  }

  _handle = () => {
    this.setState({ refreshing: true })
    setTimeout(() => {
      this.setState({ refreshing: false, products: this.state.products.reverse() })
    }, 100)

  }

  _handleRefresh = () => {
    this.setState({ refreshing: true })
    //let arr =  this.state.products.slice()

    let { products, products2 } = this.state
    if (products.length > 0 && products.length < 100) {
      let item = {}
      if (addIndex == 0) {
        item = products[addIndex++]
      } else {
        item = products.reverse()[addIndex++]
      }

      let arr = [...products]

      arr.push(item)

      setTimeout(() => {
        this.setState({ refreshing: false, products: arr.reverse() })
      }, 1000)
    } else if (products.length == 100) {
      setTimeout(() => {
        this.setState({ refreshing: false})
      }, 1200)
      this.FlatList.scrollToIndex({ index: this.state.products.length - 1 })
      this.FlatList.flashScrollIndicators()
    } else {
      setTimeout(() => {
        this.setState({ refreshing: false, products: products2})
      }, 1000)
    }
    //this.refs.FlatList.scrollToEnd()
  
  }

  _handleOnEndReached = () => {
    let { products2, products } = this.state
    if (products.length > 0 && products.length < 100) {
      let arr = [...products2]
      // arr.shift()
      let newArray = [...products, ...arr]
      this.setState({ products: newArray.reverse() })
      if(products.length == 100) alert('List is full')
    }
  }

  _handleDeletedItem = () => {
    let { products } = this.state
    if(products.length > 0) {
      let array = [...products]
      console.log(array.shift())
      this.setState({ products: array })
    }
  }

  componentWillReceiveProps(nextProps) {

    this.setState({ products: [...nextProps.productsList, ...nextProps.productsList], products2: nextProps.productsList })
    //this.setState({ products: [], products2: []})
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.products}
          keyExtractor={(item, index) => index + ''}
          renderItem={({ item, index }) => <Text style={{ fontSize: 30, fontWeight: 'bold', marginBottom: 5, marginRight: 10, backgroundColor: 'lightgray', color: 'steelblue', width: width * 0.98, textAlign: 'center' }}>{item.name}</Text>}
          contentContainerStyle={{ width: width, marginLeft: width * 0.01, alignItems: 'center', justifyContent: 'center', minHeight:(this.state.products.length > 0)? height :Dimensions.get('window').height - 50 }}
          style={{ flex: 8 }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._handleRefresh}
            />
          }
          onEndReachedThreshold={0.1}
          onEndReached={() => this._handleOnEndReached()}
          ListFooterComponent={((this.state.products.length > 0)?<Text style={{margin:5,color:'red', marginBottom:10, height:20}} onPress={() => this.FlatList.scrollToIndex({ index: 0 })}>Go to Top</Text>:<View></View>)}
          ListEmptyComponent={(<Text>List Empty</Text>)}
          ListHeaderComponent={((this.state.products.length > 0)?<Text style={{margin:5,color:'red', height:20}} onPress={()=>{this._handleDeletedItem()}}>Delete First Item</Text>:<View></View>)}
          ref={(list) => this.FlatList = list}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'skyblue',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
  },
});


const mapDispatchToProps = (dispatch) => {
  return {
    onFetchProduct: () => {
      dispatch(fetchProduct())
    },
    onGetProduct: () => {
      dispatch(FetchProduct2())
    }
  }
}

const mapStateToProps = (state) => {
  return {
    productsList: state.homeRed.products
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
