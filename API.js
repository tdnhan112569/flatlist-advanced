import axios from 'axios'
import {PRODUCT_URI, CATEGORY_URI} from './Constants'

import {FetchProduct} from './HomeAction'

const getProduct = () => {
    return axios.get(PRODUCT_URI)
}

export const fetchProduct = () => {
    return dispatch => {
        return getProduct()
        .then(response =>  response.data)
        .then(result => {
            dispatch(FetchProduct(result.body))
        })
        .catch(error => {
            console.log(error)
        })
    }
}