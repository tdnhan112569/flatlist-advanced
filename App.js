import React from 'react';
import { StyleSheet, Text, View, } from 'react-native';
import axios from 'axios'
import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import {Provider} from 'react-redux'
import HomeScreen from './HomeScreen'
import HomeReducer from './HomeReducer'

const reducer = combineReducers({homeRed: HomeReducer })
const store = createStore(reducer, applyMiddleware(thunk))

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <HomeScreen />
      </Provider>
    );
  }
}
