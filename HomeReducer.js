import {FETCH_PRODUCT, FETCH_PRODUCT_2} from './Constants'

let initstate = {
    products:[]
}

const HomeReducer = (state = initstate, action) => {
    const {type, payload} = action

    switch(type) {
        case FETCH_PRODUCT: 
            return {
                ...state,
                products:payload
            }
        case FETCH_PRODUCT_2: {
            return {
                ...state,
                products:payload.body
            }
        }    
    }

    return state
}

export default HomeReducer