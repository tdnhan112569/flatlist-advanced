import {FETCH_PRODUCT, FETCH_PRODUCT_2} from './Constants'
import data from './data'

export const FetchProduct = (products) => {
    return {
        type: FETCH_PRODUCT,
        payload:products
    }
}

export const FetchProduct2 = () => {
    return {
        type: FETCH_PRODUCT_2,
        payload:data
    }
}