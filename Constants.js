// Action
export const FETCH_PRODUCT = 'FETCH_PRODUCT'
export const FETCH_PRODUCT_2 = 'FETCH_PRODUCT_2'

//Uri

const keyController = {
    product:'/products',
    category:'/categorys'
}

const BASE_URL = 'http://api.demo.nordiccoder.com/api'


export const PRODUCT_URI = BASE_URL +  keyController.product
export const CATEGORY_URI = BASE_URL + keyController.category